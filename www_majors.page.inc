<?php

/**
 * @file www_majors.page.inc
 * Menu page callbacks for www_majors module.
 */

/**
 * Menu callback for page with embedded Degree Search application.
 *
 * Adds necessary markup to HTML head for embedded Degree Search application.
 *
 * @see www_majors_page_build()
 */
function www_majors_degree_search_page() {
  $content = array();

  $element = array(
    '#tag' => 'base',
    '#attributes' => array(
      'href' => '/' . variable_get('www_majors_degree_search_majors_path', WWW_MAJORS_DEFAULT_PATH) . '/',
    ),
  );
  drupal_add_html_head($element, 'base_url');

  return $content;
}

/**
 * Menu callback for 'degree-search/majors/refresh'.
 *
 * Validates 'access' URL parameter, invalidates Drupal & edge caches for pages
 * that reference degree search CDN assets, and regenerates cache-busting
 * request string for degree search CDN asset URLs.
 */
function www_majors_cache_refresh_page() {
  if ($access_token = variable_get('www_majors_refresh_access_token', FALSE)) {
    if (isset($_GET['access']) && $_GET['access'] == $access_token) {
      drupal_invalidate_cache_tags(array('wwt_majors:degree_search'));
      $unique_key = uniqid();
      variable_set('www_majors_cachebusting_string', $unique_key);
      drupal_add_http_header('X-Robots-Tag', 'none');
      drupal_page_is_cacheable(FALSE);
      return '<p>This page is intended for use with the degree search continuous integration system.</p>';
    }
  }

  return MENU_ACCESS_DENIED;
}
