<?php

/**
 * @file
 * www_majors module admin functions.
 */

/**
 * Form builder for www_majors settings form.
 */
function www_majors_admin_settings_form() {
  global $base_url;
  $form = array();

  $form['general'] = array(
    '#type'  => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['general']['www_majors_refresh_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Refresh cache webhook token.'),
    '#default_value' => variable_get('www_majors_refresh_access_token'),
    '#description' => t("Set the access token for the refresh cache webhook located at this address: degree-search/majors/refresh"),
  );
  $form['general']['www_majors_degree_search_majors_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the path to the degree search app.'),
    '#default_value' => variable_get('www_majors_degree_search_majors_path', WWW_MAJORS_DEFAULT_PATH),
    '#description' => t("Do not include a leading '/'. Currently the degree search app can be found here: " . $base_url . "/" . variable_get('www_majors_degree_search_majors_path', WWW_MAJORS_DEFAULT_PATH)),
    '#element_validate' => array('www_majors_degree_search_path_validate'),
  );

  $form['#submit'][] = 'www_majors_admin_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Form element validation handler for degree search path form element.
 */
function www_majors_degree_search_path_validate($element, &$form_state, $complete_form) {
  $submitted_value = $form_state['values']['www_majors_degree_search_majors_path'];
  if (empty($submitted_value)) {
    form_set_error('www_majors_degree_search_majors_path', t('A degree search path must be provided.'));
  }

  $path = strtolower(trim(trim($submitted_value), " \\/"));
  $menu_item = menu_get_item($path);
  if (!empty($path) && $submitted_value != $element['#default_value']) {
    if (!empty($menu_item)) {
      form_error($element, t('The path is already in use.'));
    }
  }
}

/**
 * Submit callback for www_majors_settings_form().
 */
function www_majors_admin_settings_form_submit($form, &$form_state) {
  variable_set('menu_rebuild_needed', TRUE);
}
